# Event-Reservation
Diese Erweiterung ergänzt das Contao-Calendar Modul um die Möglichkeit, Reservierungen zu einer Veranstaltung zu händeln. 

Es stellt ein Reservierungs-Frontend-Modul bereit das dann auf der Seite der Event-Detailseite eingebunden werden kann (da es sich den Alias aus der URL zieht um die Registrierung einem Event zuzuordnen). Man kann aber auch das Event-Reader Modul mit Reservierungslink anlegen, so das der Event mit in der URl an die andere Seite mit dem Reservierungsformular mitgegeben wird anlegen.

Man kann zu jedem Event eine maximal mögliche Anzahl der Teilnehmer, E-Mail und Subject für Reservierungsanfragen angeben.

Zu jedem Event gibt es in der Backend Event-Listen-Ansicht ein weiteres Icon, mit dem man zu der Event-Reservationseinträgen gelangt.

Ab der Version 0.2.0 werden die E-Mailtexte nicht mehr aus den Calender-Einstellungen bezogen, sondern durch das terminal42/notification_center verwaltet. In den NC-Benachrichtigungseinstellungen muss der Benachrichtigungstyp "Event-Reservierung" eingestellt werden. Dann hat man in den Texten die Möglichkeit diverse vordefinierte Token mit ##event_\*## und ##reservation_\*## zu platzieren.

In den Texten z.B. in der Eventbeschreibung, auf der Reservierungsformularseite oder erfolgreich versendet Seite, können folgende Inserttags verwendet werden:
```
{{curevent::title}}
{{curevent::startDate}}
{{curevent::endDate}}
{{curevent::startTime}}
{{curevent::endTime}}
{{curevent::max_places}}
{{curevent::open_places}}
```
