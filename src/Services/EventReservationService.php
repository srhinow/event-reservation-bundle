<?php

declare(strict_types=1);

/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 15.12.24
 */

namespace Srhinow\EventReservationBundle\Services;

use Contao\CalendarModel;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Date;
use Contao\System;
use Contao\User;
use Srhinow\EventReservationBundle\Model\CalendarEventsModel;
use Srhinow\EventReservationBundle\Model\EventReservationModel;
use Srhinow\EventReservationBundle\NotificationCenter\EventReservationToken;
use Terminal42\NotificationCenterBundle\NotificationCenter;

class EventReservationService
{
    public function __construct(
        protected ContaoFramework  $framework,
        protected ModeService      $modeService,
        private NotificationCenter $notificationCenter
    )
    {
    }

    public function getDataForInsertTags(): array
    {
        if (!$this->modeService->isFrontend()) {
            return [];
        }
        $current = time();
        if (null === $objUser = System::getContainer()->get('security.helper')->getUser()) {
            return [];
        }
        $arrData = [];

        $arrData = $this->setUserData($arrData, $objUser);

        return $arrData;
    }

    public function setUserData($arrData, User $objUser): array
    {
        $session = System::getContainer()->get('request_stack')->getSession()->getBag('contao_frontend');
        $sessionData = $session->all();

        $arrData['user_id'] = isset($sessionData['fa_user_id']) ? $sessionData['fa_user_id'] : $objUser->id;
        $arrData['user_firstname'] = isset($sessionData['fa_user_firstname']) ? $sessionData['fa_user_firstname'] : $objUser->firstname;
        $arrData['user_lastname'] = isset($sessionData['fa_user_lastname']) ? $sessionData['fa_user_lastname'] : $objUser->lastname;
        $arrData['user_fullname'] = isset($sessionData['fa_user_id'])
            ? $sessionData['fa_user_firstname'] . ' ' . $sessionData['fa_user_lastname']
            : $objUser->lastname;
        $arrData['user_cryptLogin'] = $objUser->fa_crypt_get_login;

        return $arrData;
    }

    public function sendNotificationByEventReservationId(int $eventReservationId): bool|string
    {
        /** @var EventReservationToken $eventToken */
        $eventToken = System::getContainer()->get('srhinow.eventreservation.notification_center.event_reservation_token');

        if (null === ($objEventReservation = EventReservationModel::findByPk($eventReservationId))) {
            return false;
        }
        $eventToken->setReservationToken($objEventReservation);

        if (null === ($objEvent = $objEventReservation->getRelated('pid'))) {
            return false;
        }
        $eventToken->setEventToken($objEvent);

        $token = $eventToken->getToken();

        $receipts = $this->notificationCenter->sendNotification($objEventReservation->notification, $token);

        if ($receipts->wereAllDelivered()) {
            return true;
        }

        // Otherwise, we can inspect:
        $errors = [];
        /** @var Terminal42\NotificationCenterBundle\Receipt\Receipt $receipt */

        foreach ($receipts as $receipt) {
            if (!$receipt->wasDelivered()) {
                $errors[] = $receipt->getException()->getMessage();
            }
        }

        return json_encode($errors);
    }

    /**
     * @param $key
     * @param CalendarEventsModel $objEvent
     * @return string
     */
    public function formatEventValues(string $key, CalendarEventsModel $objEvent): string
    {

        switch ($key) {
            case 'title':
                return $objEvent->title;
                break;
            case 'startDate':
                return Date::parse($GLOBALS['TL_CONFIG']['dateFormat'], $objEvent->startDate);
                break;
            case 'endDate':
                return Date::parse($GLOBALS['TL_CONFIG']['dateFormat'], $objEvent->endDate);
                break;
            case 'startTime':
                return Date::parse($GLOBALS['TL_CONFIG']['timeFormat'], $objEvent->startTime);
                break;
            case 'endTime':
                return Date::parse($GLOBALS['TL_CONFIG']['timeFormat'], $objEvent->endTime);
                break;
            case 'max_places':
                return (string)$objEvent->attendance;
                break;
            case 'open_places':
                return (string)($objEvent->attendance - $objEvent->booked);
                break;
            default:
                return (null !== $objEvent->{$key}) ? $objEvent->{$key} : '';
        }
    }

    /**
     * ersetzt Platzhalter mit entsprechende Werte im Text aus der tl_calendar_events.
     *
     * @param object $objEvent
     * @param string $text
     *
     * @return string
     */
    public function replaceEventPlaceHolder($objEvent, $text): string
    {
        preg_match_all('/\#\#([^\#]+)\#\#/', $text, $tags);

        for ($c = 0; $c < \count($tags[0]); ++$c) {
            $text = str_replace($tags[0][$c], $this->formatEventValues($tags[1][$c], $objEvent));
        }

        return $text;
    }

}
