<?php

declare(strict_types=1);


namespace Srhinow\EventReservationBundle\Services;

use Contao\CoreBundle\Routing\ScopeMatcher;
use Contao\CoreBundle\Security\Authentication\Token\TokenChecker;
use Contao\User;
use Symfony\Component\HttpFoundation\RequestStack;

readonly class ModeService
{
    public function __construct(
        private RequestStack $requestStack,
        private ScopeMatcher $scopeMatcher,
        private tokenChecker $tokenChecker,
    ) {
    }

    public function isBackend(): bool
    {
        return $this->scopeMatcher->isBackendRequest($this->requestStack->getCurrentRequest());
    }

    public function isFrontend()
    {
        return $this->scopeMatcher->isFrontendRequest($this->requestStack->getCurrentRequest());
    }

    public function getMode(): bool
    {
        if(!$this->scopeMatcher->isContaoRequest($this->requestStack->getCurrentRequest())){
            return '';
        }

        return $this->isBackend() ? 'BE' : 'FE';
    }

    public function hasFrontendUser(): bool
    {
        return $this->tokenChecker->hasFrontendUser();
    }

}
