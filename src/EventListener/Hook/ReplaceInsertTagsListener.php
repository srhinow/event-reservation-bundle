<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

namespace Srhinow\EventReservationBundle\EventListener\Hook;

use Contao\Config;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\Input;
use Srhinow\EventReservationBundle\Model\CalendarEventsModel;
use Srhinow\EventReservationBundle\Services\EventReservationService;

/**
 * Handles insert tags for event-reservations.
 */
class ReplaceInsertTagsListener
{
    public function __construct(
        private ContaoFramework         $framework,
        private EventReservationService $eventReservationService
    )
    {
    }

    public function onInsertTagsListener(
        string $insertTag,
        bool   $useCache,
        string $cachedValue,
        array  $flags,
        array  $tags,
        array  $cache,
        int    $_rit,
        int    $_cnt
    )
    {

        if ('curevent::' === substr($insertTag, 0, 10)) {

            return $this->replaceCurrentEventDetails($insertTag);
        }

        return false;
    }

    /**
     * replace Details from Service-People.
     *
     * @return bool
     */
    public function replaceCurrentEventDetails($strTag): bool|string
    {
        // Set the item from the auto_item parameter
        if (!isset($_GET['events']) && Config::get('useAutoItem') && isset($_GET['auto_item'])) {
            Input::setGet('events', Input::get('auto_item'));
        }

        // Do not index or cache the page if no event has been specified
        if (!Input::get('events')) {
            /* @var \PageModel $objPage */
            global $objPage;

            $objPage->noSearch = 1;
            $objPage->cache = 0;

            return false;
        }

        if (stristr($strTag, 'curevent::')) {
            $parts = explode('::', $strTag);
            if (null === ($objEvent = CalendarEventsModel::findPublishedByIdOrAlias(Input::get('events')))) {
                return false;
            }

            return $this->eventReservationService->formatEventValues($parts[1], $objEvent);
        }

        return false;
    }
}
