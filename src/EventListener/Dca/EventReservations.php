<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

namespace Srhinow\EventReservationBundle\EventListener\Dca;

use Contao\Backend;
use Contao\CalendarModel;
use Contao\Controller;
use Contao\CoreBundle\Exception\AccessDeniedException;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\DataContainer;
use Contao\Date;
use Contao\Email;
use Contao\Image;
use Contao\Input;
use Contao\Message;
use Contao\StringUtil;
use Contao\System;
use Contao\Versions;
use Psr\Log\LogLevel;
use Srhinow\EventReservationBundle\Model\CalendarEventsModel;
use Srhinow\EventReservationBundle\Model\EventReservationModel;
use Srhinow\EventReservationBundle\NotificationCenter\Type\EventReservationNotificationType;
use Srhinow\EventReservationBundle\Services\EventReservationService;
use Terminal42\NotificationCenterBundle\NotificationCenter;
use Valid\FaKonfiguratorBundle\NotificationCenter\Type\FensterartNotificationType;

class EventReservations extends Backend
{
    /**
     * @var object|\Symfony\Bridge\Monolog\Logger|null
     */
    protected $logger;

    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('BackendUser', 'User');
        $this->logger = static::getContainer()->get('monolog.logger.contao');
    }

    /**
     * Check permissions to edit table tl_event_reservations.
     *
     * @throws \Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function checkPermission(): void
    {
        $bundles = System::getContainer()->getParameter('kernel.bundles');

        if ($this->User->isAdmin) {
            return;
        }

        // Set root IDs
        if (empty($this->User->event_reservationss) || !\is_array($this->User->event_reservationss)) {
            $root = [0];
        } else {
            $root = $this->User->event_reservationss;
        }

        // den Button "neuer Fall" ausblenden wenn die notwendigen Berechtigungen fehlen
        if (!$this->User->hasAccess('create', 'event_reservationsp')) {
            $GLOBALS['TL_DCA']['tl_spree_case']['config']['closed'] = true;
        }

        /** @var \Symfony\Component\HttpFoundation\Session\SessionInterface $objSession */
        $objSession = System::getContainer()->get('session');

        // Check current action
        switch (Input::get('act')) {
            case 'select':
            case 'show':
            case '':
                // Allow
                break;
            case 'edit':
                // Check permissions to add event_reservationss
                if (!$this->User->hasAccess('edit', 'event_reservationsp')) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' event reservation ID ' . Input::get('id') . '.');
                }
                break;
            case 'create':
                // Check permissions to add event_reservationss
                if (!$this->User->hasAccess('create', 'event_reservationsp')) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' event reservation ID ' . Input::get('id') . '.');
                }
                break;
            case 'copy':
                // Check permissions to add event_reservationss
                if (!$this->User->hasAccess('copy', 'event_reservationsp')) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' event reservation ID ' . Input::get('id') . '.');
                }
            // no break
            case 'delete':
                // Check permissions to add event_reservationss
                if (!$this->User->hasAccess('delete', 'event_reservationsp')) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' event reservation ID ' . Input::get('id') . '.');
                }
                break;

            case 'editAll':
            case 'deleteAll':
            case 'overrideAll':
                $session = $objSession->all();
                if ('deleteAll' === Input::get('act') && !$this->User->hasAccess('delete', 'event_reservationsp')) {
                    $session['CURRENT']['IDS'] = [];
                } else {
                    $session['CURRENT']['IDS'] = array_intersect((array)$session['CURRENT']['IDS'], $root);
                }
                $objSession->replace($session);
                break;

            default:
                if (\strlen(Input::get('act')) > 0) {
                    throw new AccessDeniedException('Not enough permissions to ' . Input::get('act') . ' event_reservationss.');
                }
                break;
        }
    }

    /**
     * List a recipient.
     *
     * @param array $row
     *
     * @return string
     */
    public function listRecipient($row)
    {
        $label = $row['firstname'] . ' ' . $row['lastname'];
        $label .= ' (' . ($row['library'] ? $row['library'] . ', ' : '') . $row['email'] . ')';

        // '1' => 'bestätigt'
        if ('1' === $row['status']) {
            $label .= ' <span style="color:#b3b3b3;padding-left:3px">('
                . $GLOBALS['TL_LANG']['tl_event_reservations']['participation_accepted']
                . ' am ' . Date::parse($GLOBALS['TL_CONFIG']['datimFormat'], $row['statusDate'])
                . ')</span>';
            // '2' => 'abgelehnt'
        } elseif ('2' === $row['status']) {
            $label .= ' <span style="color:#b3b3b3;padding-left:3px">('
            . $GLOBALS['TL_LANG']['tl_event_reservations']['participation_rejected']
            . ' am ' . Date::parse($GLOBALS['TL_CONFIG']['datimFormat'], $row['statusDate'])
            . ')</span>';
            // '3' => 'Warteliste'
        } elseif ('3' === $row['status']) {
            $label .= ' <span style="color:#b3b3b3;padding-left:3px">('
            . $GLOBALS['TL_LANG']['tl_event_reservations']['participation_waiting_list']
            . ' am ' . Date::parse($GLOBALS['TL_CONFIG']['datimFormat'], $row['statusDate'])
            . ')</span>';
        } else {
            $label .= ' <span style="color:#b3b3b3;padding-left:3px">('
                . $GLOBALS['TL_LANG']['tl_event_reservations']['participation_no_accepted']
                . ')</span>';
        }

        $tmpl = '<div style="float:left">';
        $tmpl .= '<div class="list_icon" style="background-image:url(\'%ssystem/themes/%s/images/%s.gif\')">%s</div>';
        $tmpl .= '</div>';

        return sprintf(
                $tmpl,
                System::getContainer()->get('contao.assets.assets_context')->getStaticUrl(),
                $this->getTheme(),
                ($row['status'] ? 'member' : 'member_'),
                $label,
                rtrim($label, '_'),
                rtrim($label, '_')
            ) . "\n";
    }


    /**
     * Return the "toggle visibility" button.
     *
     * @param array $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public
    function toggleIcon($row, $href, $label, $title, $icon, $attributes): string
    {
        if (\strlen(Input::get('tid'))) {
            $this->toggleVisibility(Input::get('tid'), (1 === Input::get('state')));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_event_reservations::status', 'alexf')) {
            return '';
        }

        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['status'] ? '' : 1);

        if (!$row['status']) {
            $icon = 'invisible.gif';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '" ' . $attributes . '>'
            . Image::getHtml($icon, $label)
            . '</a> ';
    }

    /**
     * Disable/enable a user group.
     *
     * @param int $intId
     * @param bool $blnVisible
     */
    public
    function toggleVisibility($intId, $blnVisible): void
    {
        // Check permissions to edit
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');
        $this->checkPermission();

        // Check permissions to publish
        if (!$this->User->isAdmin && !$this->User->hasAccess('tl_event_reservations::status', 'alexf')) {
            $this->logger->addError(
                'Not enough permissions to publish/unpublish event reservation ID "' . $intId . '"'
            );

            $this->redirect('contao/main.php?act=error');
        }

        $objVersions = new Versions('tl_event_reservations', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (\is_array($GLOBALS['TL_DCA']['tl_event_reservations']['fields']['status']['save_callback'])) {
            foreach ($GLOBALS['TL_DCA']['tl_event_reservations']['fields']['status']['save_callback'] as $callback) {
                $this->import($callback[0]);
                $blnVisible = $this->$callback[0]->$callback[1]($blnVisible, $this);
            }
        }

        // Update the database
        $objEventReservation = EventReservationModel::findByPk($intId);
        $objEventReservation->modify = time();
        $objEventReservation->status = ($blnVisible ? 1 : '');
        $objEventReservation->save();

        $objVersions->create();
    }

    /**
     * update Entry-Dataset.
     */
    public function UpdateEntry($varValue, DataContainer $dc)
    {
        if ($varValue !== $dc->activeRecord->status) {
            if (null === ($objEventReservation = EventReservationModel::findByPk($dc->id))) {
                return $varValue;
            }

            $objEventReservation->modify = time();
            $objEventReservation->statusDate = time();
            $objEventReservation->save();

            //Update Reservation-Counter in Parent-Event
            $this->updateReservationCount($dc);

        }

        return $varValue;
    }

    public
    function updateReservationCount(DataContainer $dc): void
    {
        if ($dc->activeRecord->pid) {
            $count = EventReservationModel::countBy(['pid=?', 'status=?'], [$dc->activeRecord->pid, 1]);

            $objEvent = CalendarEventsModel::findByPk($dc->activeRecord->pid);
            $objEvent->booked = $count;
            $objEvent->save();
        }
    }

    /**
     * send a confirmation email to the event participants.
     *
     * @param object $dc
     */
    public
    function sendReservationEmail(DataContainer $dc): void
    {
        if ((int)($dc->activeRecord->sendEmail) < 1) {
            return;
        }

        $eventReservationService = System::getContainer()
            ->get('srhinow.eventreservation.services.event_reservation_service');

        $feedback = $eventReservationService->sendNotificationByEventReservationId((int)$dc->id);
        if (true === $feedback) {
            Message::addConfirmation($GLOBALS['TL_LANG']['MSC']['messages']['nc_send_succesfully']);

            $logger = static::getContainer()->get('monolog.logger.contao');
            $strText = sprintf(
                'SuccessFully send Notification %s by event reservation ID "%s"',
                $dc->activeRecord->notification,
                $dc->id
            );
            $logger->info($strText, array('contao' => new ContaoContext(__FUNCTION__, ContaoContext::ACCESS)));

            $objEventReservation = EventReservationModel::findByPk($dc->id);
            $objEventReservation->sendEmail = '';
            $objEventReservation->save();
        } else {
            Message::addConfirmation($GLOBALS['TL_LANG']['MSC']['messages']['nc_send_error']);

            $logger = static::getContainer()->get('monolog.logger.contao');
            $strText = sprintf(
                'Error: the Notification %s by event reservation ID "%s"',
                $dc->activeRecord->notification,
                $dc->id
            );
            $logger->error($strText, array('contao' => new ContaoContext(__FUNCTION__, ContaoContext::ERROR)));
        }
    }
}
