<?php

declare(strict_types=1);

/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 26.12.24
 */

namespace Srhinow\EventReservationBundle\EventListener\Dca;

use Contao\CoreBundle\DependencyInjection\Attribute\AsCallback;
use Contao\DataContainer;
use Srhinow\EventReservationBundle\NotificationCenter\Type\EventReservationNotificationType;
use Terminal42\NotificationCenterBundle\NotificationCenter;
use Valid\FaKonfiguratorBundle\NotificationCenter\Type\FensterartNotificationType;

class EventReservationNotificationOptions
{
    public function __construct(private readonly NotificationCenter $notificationCenter)
    {
    }

    /**
     * @return array<string>
     */
    #[AsCallback(table: 'tl_event_reservations', target: 'fields.notification.options')]
    public function onNotificationOptionsCallback(DataContainer $dc): array
    {
        return $this->notificationCenter->getNotificationsForNotificationType(EventReservationNotificationType::NAME);
    }
}
