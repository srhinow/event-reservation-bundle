<?php

declare(strict_types=1);

/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 15.12.24
 */

namespace Srhinow\EventReservationBundle\EventListener\Dca;

use Contao\Backend;
use Contao\DataContainer;
use Srhinow\EventReservationBundle\NotificationCenter\Type\EventReservationNotificationType;
use Terminal42\NotificationCenterBundle\NotificationCenter;

readonly class Calendar
{
    /**
     * Import the back end user object.
     */
    public function __construct(private NotificationCenter $notificationCenter)
    {}

    public function onNotificationOptionsCallback(DataContainer $dc): array
    {
        return $this->notificationCenter->getNotificationsForNotificationType(EventReservationNotificationType::NAME);
    }
}
