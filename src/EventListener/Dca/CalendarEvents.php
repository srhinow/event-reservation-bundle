<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

namespace Srhinow\EventReservationBundle\EventListener\Dca;

use Contao\Backend;
use Contao\Calendar;
use Contao\DataContainer;
use Contao\Date;

class CalendarEvents extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add the type of input field.
     *
     * @param array $arrRow
     *
     * @return string
     */
    public function listEvents($arrRow): string
    {
        $time = time();
        $key = ($arrRow['published']
            && ('' === $arrRow['start'] || $arrRow['start'] < $time)
            && ('' === $arrRow['stop'] || $arrRow['stop'] > $time)) ? 'published' : 'unpublished';

        $span = Calendar::calculateSpan($arrRow['startTime'], $arrRow['endTime']);

        if ($span > 0) {
            $date = Date::parse(
                $GLOBALS['TL_CONFIG'][($arrRow['addTime'] ? 'datimFormat' : 'dateFormat')],
                $arrRow['startTime']
                )
                .' - '
                .Date::parse(
                    $GLOBALS['TL_CONFIG'][($arrRow['addTime'] ? 'datimFormat' : 'dateFormat')],
                    $arrRow['endTime']
                );
        } elseif ($arrRow['startTime'] === $arrRow['endTime']) {
            $date = Date::parse(
                $GLOBALS['TL_CONFIG']['dateFormat'],
                $arrRow['startTime']
                )
                .$arrRow['addTime']
                    ? ' ('.Date::parse($GLOBALS['TL_CONFIG']['timeFormat'], $arrRow['startTime']).')'
                    : '';
        } else {
            $date =
                Date::parse($GLOBALS['TL_CONFIG']['dateFormat'], $arrRow['startTime'])
                .($arrRow['addTime']
                    ? ' ('
                    .Date::parse($GLOBALS['TL_CONFIG']['timeFormat'], $arrRow['startTime'])
                    .' - '
                    .Date::parse($GLOBALS['TL_CONFIG']['timeFormat'], $arrRow['endTime'])
                    .')'
                    : '');
        }

        return '
			<div class="cte_type '.$key.'"><strong>'.$arrRow['title'].'</strong>
			<br>Datum / Zeitraum: '.$date.'
			<br>verfügbare Plätze: '.$arrRow['attendance'].', bereits gebucht: '.$arrRow['booked'].'
			</div>
			<div class="limit_height'.(!$GLOBALS['TL_CONFIG']['doNotCollapse'] ? ' h52' : '').'">
			'.(('' !== $arrRow['details']) ? $arrRow['details'] : $arrRow['teaser']).'
			</div>'."\n";
    }

    public function updateReservationCount(DataContainer $dc): void
    {
        if ($dc->activeRecord->pid) {
            $rcountObj = $this->Database->prepare('SELECT count(*) as `c` FROM `tl_event_reservations` WHERE `pid`=? AND `status`=?')
                ->limit(1)
                ->execute($dc->activeRecord->id, 1)
            ;

            $this->Database->prepare('UPDATE `tl_calendar_events` SET `booked`=? WHERE `id`=?')
                ->limit(1)
                ->execute($rcountObj->c, $dc->activeRecord->id)
            ;
        }
    }
}
