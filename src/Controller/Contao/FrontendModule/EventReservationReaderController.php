<?php

declare(strict_types=1);

/**
 * Created by bzn-event_reservation.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 27.12.24
 */

namespace Srhinow\EventReservationBundle\Controller\Contao\FrontendModule;

use Contao\Calendar;
use Contao\CalendarEventsModel;
use Contao\Config;
use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\Exception\PageNotFoundException;
use Contao\CoreBundle\Security\Authentication\Token\TokenChecker;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\CoreBundle\String\HtmlDecoder;
use Contao\Date;
use Contao\Environment;
use Contao\Events;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\Input;
use Contao\ModuleModel;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Contao\Template;
use Srhinow\EventReservationBundle\Services\ModeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

/**
 * @FrontendModule("eventreservationreader",
 *   category="event_reservation",
 *   template="mod_eventreader",
 *   renderer="forward"
 * )
 */
class EventReservationReaderController extends AbstractFrontendModuleController
{
    protected ?PageModel $objPage;

    // if current user already booked
    protected bool $alreadyReserv = false;

    public function __construct(
        protected ModeService $modeService,
        protected HtmlDecoder $htmlDecoder,
        protected Security    $security
    )
    {
    }

    protected function getResponse(Template $template, ModuleModel $model, Request $request): Response
    {
        global $objPage;
        $this->objPage = $objPage;

        // Fallback template
        if (\strlen($model->customTpl)) {
            $template->strTemplate = $model->customTpl;
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['events']) && Config::get('useAutoItem') && isset($_GET['auto_item'])) {
            Input::setGet('events', Input::get('auto_item'));
        }

        $template->event = '';
        $template->referer = 'javascript:history.go(-1)';
        $template->back = $GLOBALS['TL_LANG']['MSC']['goBack'];

        $time = time();

        // Get the current event
        $objEvent = CalendarEventsModel::findPublishedByParentAndIdOrAlias(Input::get('events'), StringUtil::deserialize($model->cal_calendar));

        // The event does not exist (see #33)
        if ($objEvent === null) {
            throw new PageNotFoundException('Page not found: ' . Environment::get('uri'));
        }

        $user = $this->security->getUser();

        if ($this->modeService->hasFrontendUser()) {
            $objReserv = $this->Database->prepare('SELECT * FROM `tl_event_reservations` WHERE `member_id`=? AND `pid`=?')
                ->limit(1)
                ->execute($user->id, $objEvent->id);

            $this->alreadyReserv = ($objReserv->numRows > 0) ? true : false;
        }

        // Overwrite the page title
        if ('' !== (string)$objEvent->title) {
            $objPage->pageTitle = StringUtil::stripInsertTags($objEvent->title);
        }

        // Overwrite the page description
        if ('' !== (string)$objEvent->teaser) {
            $objPage->description = $this->htmlDecoder->htmlToPlainText($objEvent->teaser);
        }

        $span = Calendar::calculateSpan($objEvent->startTime, $objEvent->endTime);

        $strTimeStart = '<time datetime="' . date('Y-m-d\TH:i:sP', (int)($objEvent->startTime)) . '">';
        $strTimeEnd = '<time datetime="' . date('Y-m-d\TH:i:sP', (int)($objEvent->endTime)) . '">';
        $strTimeClose = '</time>';

        // Get date
        if ($span > 0) {
            $date = $strTimeStart . Date::parse(($objEvent->addTime ? $objPage->datimFormat : $objPage->dateFormat), $objEvent->startTime) . $strTimeClose . ' - ' . $strTimeEnd . Date::parse(($objEvent->addTime ? $objPage->datimFormat : $objPage->dateFormat), $objEvent->endTime) . $strTimeClose;
        } elseif ($objEvent->startTime === $objEvent->endTime) {
            $date = $strTimeStart . Date::parse($objPage->dateFormat, $objEvent->startTime) . ($objEvent->addTime ? ' (' . Date::parse($objPage->timeFormat, $objEvent->startTime) . ')' : '') . $strTimeClose;
        } else {
            $date = $strTimeStart . Date::parse($objPage->dateFormat, $objEvent->startTime) . ($objEvent->addTime ? ' (' . Date::parse($objPage->timeFormat, $objEvent->startTime) . $strTimeClose . ' - ' . $strTimeEnd . Date::parse($objPage->timeFormat, $objEvent->endTime) . ')' : '') . $strTimeClose;
        }

        $until = '';
        $recurring = '';

        // Recurring event
        if ($objEvent->recurring) {
            $arrRange = StringUtil::deserialize($objEvent->repeatEach);
            $strKey = 'cal_' . $arrRange['unit'];
            $recurring = sprintf($GLOBALS['TL_LANG']['MSC'][$strKey], $arrRange['value']);

            if ($objEvent->recurrences > 0) {
                $until = sprintf($GLOBALS['TL_LANG']['MSC']['cal_until'], Date::parse($objPage->dateFormat, $objEvent->repeatEnd));
            }
        }

        // Override the default image size
        if ('' !== $model->imgSize) {
            $size = StringUtil::deserialize($model->imgSize);

            if ($size[0] > 0 || $size[1] > 0) {
                $objEvent->size = $model->imgSize;
            }
        }

        $objTemplate = new FrontendTemplate($model->cal_template);
        $objTemplate->setData($objEvent->row());
        $objTemplate->alreadyReserv = $this->alreadyReserv;
        $objTemplate->date = $date;
        $objTemplate->start = $objEvent->startTime;
        $objTemplate->end = $objEvent->endTime;
        $objTemplate->class = ('' !== $objEvent->cssClass) ? ' ' . $objEvent->cssClass : '';
        $objTemplate->recurring = $recurring;
        $objTemplate->until = $until;


        // Get current "jumpTo" Reservation Form page
        $objDetailPage = PageModel::findByIdOrAlias($model->jumpTo);
        if (null !== $objDetailPage) {
            $objUrlGenerator = System::getContainer()->get('contao.routing.url_generator');

            $objTemplate->ReservationLink = $objUrlGenerator->generate(
                ($objDetailPage->alias ?: $this->id) . '/' . Input::get('events'),
                [
                    '_locale' => $objPage->rootLanguage,
                    '_domain' => $objPage->domain,
                    '_ssl' => (bool)$objPage->rootUseSSL,
                ]
            );
        } else {
            $objTemplate->ReservationLink = preg_replace('/\?.*$/i', '', $this->Environment->request);
        }

        // Clean the RTE output
        $objEvent->details = StringUtil::toHtml5($objEvent->details);

        $objTemplate->details = StringUtil::encodeEmail($objEvent->details);
        $objTemplate->addImage = false;

        // Add an image
        if ($objEvent->addImage && $objEvent->singleSRC) {
            $objModel = FilesModel::findByUuid($objEvent->singleSRC);

            if ($objModel !== null && is_file(System::getContainer()->getParameter('kernel.project_dir') . '/' . $objModel->path)) {
                // Do not override the field now that we have a model registry (see #6303)
                $arrEvent = $objEvent->row();

                // Override the default image size
                if ($this->imgSize) {
                    $size = StringUtil::deserialize($this->imgSize);

                    if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2]) || ($size[2][0] ?? null) === '_') {
                        $arrEvent['size'] = $this->imgSize;
                    }
                }

                $arrEvent['singleSRC'] = $objModel->path;
                $this->addImageToTemplate($objTemplate, $arrEvent, null, null, $objModel);
            }
        }

        $objTemplate->enclosure = array();

        // Add enclosures
        if ($objEvent->addEnclosure) {
            $this->addEnclosuresToTemplate($objTemplate, $objEvent->row());
        }

        // schema.org information
        $objTemplate->getSchemaOrgData = static function () use ($objTemplate, $objEvent): array
        {
            $jsonLd = Events::getSchemaOrgData($objEvent);

            if ($objTemplate->addImage && $objTemplate->figure)
            {
                $jsonLd['image'] = $objTemplate->figure->getSchemaOrgData();
            }

            return $jsonLd;
        };

        $template->event = $objTemplate->parse();

        // Tag the event (see #2137)
        if (System::getContainer()->has('fos_http_cache.http.symfony_response_tagger'))
        {
            $responseTagger = System::getContainer()->get('fos_http_cache.http.symfony_response_tagger');
            $responseTagger->addTags(array('contao.db.tl_calendar_events.' . $objEvent->id));
        }

        return $template->getResponse();
    }

}
