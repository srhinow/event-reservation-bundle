<?php

declare(strict_types=1);

/**
 * Created by event_reservation.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 15.12.24
 */

namespace Srhinow\EventReservationBundle\Controller\Contao\FrontendModule;

use Contao\Config;
use Contao\Controller;
use Contao\CoreBundle\Controller\FrontendModule\AbstractFrontendModuleController;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\CoreBundle\ServiceAnnotation\FrontendModule;
use Contao\Email;
use Contao\Environment;
use Contao\FormCaptcha;
use Contao\Input;
use Contao\ModuleModel;
use Contao\PageModel;
use Contao\System;
use Contao\Template;
use Contao\Widget;
use Srhinow\EventReservationBundle\Model\CalendarEventsModel;
use Srhinow\EventReservationBundle\Model\EventReservationModel;
use Srhinow\EventReservationBundle\Services\ModeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Terminal42\NotificationCenterBundle\NotificationCenter;
use Srhinow\EventReservationBundle\NotificationCenter\EventReservationToken;
use Srhinow\EventReservationBundle\Services\EventReservationService;
use Symfony\Component\Routing\RouterInterface;

/**
 * @FrontendModule("eventreservationform",
 *   category="event_reservation",
 *   template="event_reservation_form",
 *   renderer="forward"
 * )
 */
class EventReservationFormController extends AbstractFrontendModuleController
{
    protected ?PageModel $objPage;

    /**
     * Target pages.
     *
     * @var array
     */
    protected $arrWidgets = [];

    protected bool $doNotSubmit;

    protected string $strFormId;

    /**
     * @param RouterInterface $router
     * @param EventReservationService $service
     * @param ModeService $modeService
     * @param EventReservationToken $eventToken
     * @param NotificationCenter $notificationCenter
     */
    public function __construct(
        protected ContaoFramework         $framework,
        protected RouterInterface         $router,
        protected EventReservationService $service,
        protected ModeService             $modeService,
        protected EventReservationToken   $eventToken,
        private NotificationCenter        $notificationCenter
    )
    {
    }

    protected function getResponse(Template $template, ModuleModel $model, Request $request): Response
    {
        global $objPage;
        $this->objPage = $objPage;

        $this->doNotSubmit = false;
        $this->strFormId = 'event_reservation_form_' . $model->id;

        Controller::loadLanguageFile('tl_event_reservations');
        $tokenName = System::getContainer()->getParameter('contao.csrf_token_name');
        $tokenManager = System::getContainer()->has('contao.csrf.token_manager') ? System::getContainer()->get('contao.csrf.token_manager') : System::getContainer()->get('security.csrf.token_manager');

        // Fallback template
        if (strlen($model->cal_template)) {
            $template->strTemplate = $model->cal_template;
        }

        // Set the item from the auto_item parameter
        if (!isset($_GET['events']) && Config::get('useAutoItem') && isset($_GET['auto_item'])) {
            Input::setGet('events', Input::get('auto_item'));
        }

        $this->getWidgetsForTemplate($model);

        //wenn Formular abgesendet wurde
        if (Input::post('FORM_SUBMIT') === $this->strFormId && !$this->doNotSubmit) {
            $objReservation = $this->createNewReservation();
            $this->sendReservationEmail($objReservation, $model);

            if ($model->jumpTo) {
                $obJumpTo = PageModel::findWithDetails($model->jumpTo);
                $urlJumpTo = $obJumpTo->getAbsoluteUrl((Config::get('useAutoItem') && !Config::get('disableAlias')) ? '/%s' : '/events/%s');
                $finishReservationUrl = sprintf($urlJumpTo, Input::get('events'));
                Controller::redirect($finishReservationUrl);
            } else {
                Controller::reload();
            }
        }

        $template->headline = unserialize($model->headline)['value'];
        $template->fields = $this->arrWidgets;
        $template->submit = $GLOBALS['TL_LANG']['tl_event_reservations']['form_submit_title'];
        $template->action = ampersand($request->getRequestUri());
        $template->messages = ''; // Backwards compatibility
        $template->formId = $this->strFormId;
        $template->hasError = $this->doNotSubmit;
        $template->requestToken = System::getContainer()->get('contao.csrf.token_manager')->getDefaultTokenValue();

        return $template->getResponse();
    }

    protected function getWidgetsForTemplate($model): array
    {
        // hole alle fürs Frontend aktivierten Felder aus der Reservation-Tabelle
        $arrFields = $this->getFrontendFields($model);

        $row = 0;

        // Initialize widgets
        foreach ($arrFields as $name => $field) {

            /** @var Widget $strClass */
            $strClass = $GLOBALS['TL_FFL'][$field['inputType']];

            // Continue if the class is not defined
            if (!class_exists($strClass)) {
                continue;
            }

            if (isset($field['eval']['mandatory'])) {
                $field['eval']['required'] = $field['eval']['mandatory'];
            }
            if ($name === 'captcha') {
                $objWidget = new FormCaptcha(FormCaptcha::getAttributesFromDca($field, $name));
            } else {
                /** @var Widget $objWidget */
                $objWidget = new $strClass(Widget::getAttributesFromDca($field, $name));
                $objWidget->name = $name;
                $objWidget->value = isset($field['value']) ? $field['value'] : '';
                $objWidget->storeValues = true;
                $objWidget->rowClass = 'row_' . $row . ((0 === $row) ? ' row_first' : '') . ((0 === ($row % 2)) ? ' even' : ' odd');
                ++$row;
            }

            // Validate the widget
            if (Input::post('FORM_SUBMIT') === $this->strFormId) {
                if ($name !== 'captcha') {
                    $objWidget->value = Input::post($name);
                }

                $objWidget->validate();

                if ($objWidget->hasErrors()) {
                    $this->doNotSubmit = true;
                }
            }

            $this->arrWidgets[$name] = $objWidget;
        }

        return $this->arrWidgets;
    }

    /**
     * @return array
     */
    protected function getFrontendFields(ModuleModel $model): array
    {
        $arrFields = [];
        Controller::loadDataContainer('tl_event_reservations');
        $user = System::getContainer()->get('security.token_storage')->getToken()->getUser();

        $arrReservationFields = $GLOBALS['TL_DCA']['tl_event_reservations']['fields'];

        if (!is_array($arrReservationFields) || count($arrReservationFields) < 1) {
            return $arrFields;
        }

        foreach ($arrReservationFields as $name => $data) {

            if (!isset($data['eval']['feViewable']) || false === (bool)$data['eval']['feViewable']) {
                continue;
            }
            $arrFields[$name] = $data;

            //Wenn es das Feld auch im angemeldeten Member gibt den Wert vorbelegen
            if ($this->modeService->hasFrontendUser()) {

                $user = System::getContainer()->get('security.token_storage')->getToken()->getUser();
                try {
                    $arrFields[$name]['value'] = trim((string)$user->{$name});
                } catch (\Exception $e) {

                }


            }
        }

        if (!$this->modeService->hasFrontendUser()) {
            $arrFields['captcha'] = [
                'name' => 'subscribe_' . $model->id,
                'label' => $GLOBALS['TL_LANG']['MSC']['securityQuestion'],
                'inputType' => 'captcha',
                'eval' => ['mandatory' => true],
            ];
        }

        // HOOK: change Reservation-Fields before add as Widgets
        if (isset($GLOBALS['TL_HOOKS']['EventReservationFields'])
            && is_array($GLOBALS['TL_HOOKS']['EventReservationFields'])) {
            foreach ($GLOBALS['TL_HOOKS']['EventReservationFields'] as $callback) {
                $arrFields = System::importStatic($callback[0])->{$callback[1]}($arrFields);
            }
        }

        return $arrFields;
    }

    /**
     * @return EventReservationModel
     */
    protected function createNewReservation()
    {
        $objEvent = CalendarEventsModel::findPublishedByIdOrAlias(Input::get('events'));

        $objReservation = new EventReservationModel();
        $objReservation->pid = $objEvent->id;
        if (null === ($user = System::getContainer()->get('security.token_storage')->getToken()->getUser())) {
            $objReservation->member_id = (int)$user->id;
        }

        $objReservation->tstamp = time();
        $objReservation->statusDate = time();

        if (is_array($this->arrWidgets) && count($this->arrWidgets) > 0) {
            foreach ($this->arrWidgets as $name => $obj) {
                $objReservation->{$name} = $this->arrWidgets[$name]->value;
            }
        }

        $objReservation->save();

        return $objReservation;
    }


    /**
     * @throws \Exception
     */
    protected function sendReservationEmail(EventReservationModel $objReservation, ModuleModel $model): void
    {
        if (null === $objReservation) {
            return;
        }
        if (null === ($objEvent = CalendarEventsModel::findByPk($objReservation->pid))) {
            return;
        }

        if (null === ($objCal = $objEvent->getRelated('pid'))) {
            return;
        }

        /** @var Environment $environment */
        $environment = $this->framework->getAdapter(Environment::class);

        $objEmail = new Email();
        $objEmail->from = $this->arrWidgets['email']->value;
        $objEmail->fromName = $this->arrWidgets['firstname']->value . ' ' . $this->arrWidgets['lastname']->value;

        //(Eventeinstellung vor Moduleinstellung)
        $objEmail->subject = ($objEvent->ereserv_formto_subject) ?: $objCal->ereserv_subject;
        $objEmail->text = 'Datensatz-ID: ' . $objReservation->id . "\n";
        $objEmail->text .= 'Event: ' . $objEvent->title . ' (' . $objEvent->id . ')' . "\n";
        $objEmail->text .= 'Host: ' . $environment->get('host') . '"' . "\n";
        $objEmail->text .= 'Backend-Link: ' . $environment->get('url') . '/contao?do=calendar&amp;table=tl_event_reservations&amp;id=' . $objEvent->id . "\n";
        $objEmail->text .= "\n----------------------------------\n";
        foreach ($this->arrWidgets as $k => $obj) {
            $objEmail->text .= $k . ': ' . $obj->value . "\n";
        }
        $objEmail->text .= "\n----------------------------------\n";

        //Email senden (Eventeinstellung vor Kalender-Einstellung)
        $emailTo = ($objEvent->ereserv_formto_email) ?: $objCal->ereserv_sender;
        if (null !== $emailTo) {
            $objEmail->sendTo($emailTo);
        }
    }
}
