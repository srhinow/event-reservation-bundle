<?php

declare(strict_types=1);

/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 15.12.24
 */

namespace Srhinow\EventReservationBundle\NotificationCenter;

use Contao\Date;
use Contao\FrontendUser;
use Contao\Input;
use Srhinow\EventReservationBundle\Model\CalendarEventsModel;
use Srhinow\EventReservationBundle\Model\EventReservationModel;
use Srhinow\EventReservationBundle\Services\EventReservationService;

class EventReservationToken
{
    protected array $token = [];

    public function __construct(
        protected EventReservationService $eventReservationService,
    )
    {
    }

    public function getToken(): array
    {
        return $this->token;
    }

    public function setToken($key, $value): void
    {
        $this->token[$key] = $value;
    }

    public function setReservationToken(EventReservationModel $objReservation, $tokenPrefix = 'reservation_'): void
    {
        $fields = ['firstname', 'lastname', 'street', 'postal', 'email', 'phone'];

        foreach ($fields as $field) {
            $this->setToken($tokenPrefix . $field, $objReservation->{$field});
        }
    }

    public function setEventToken(CalendarEventsModel $objEvent, $tokenPrefix = 'event_'): void
    {
        $fields = ['title', 'startDate', 'endDate', 'startTime', 'endTime','max_places','open_places'];

        foreach ($fields as $field) {
            $this->setToken($tokenPrefix . $field, $this->eventReservationService->formatEventValues($field, $objEvent));
        }
    }

}
