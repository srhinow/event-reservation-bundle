<?php

declare(strict_types=1);

/**
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 15.12.24
 */

namespace Srhinow\EventReservationBundle\NotificationCenter\Type;


use Terminal42\NotificationCenterBundle\NotificationType\NotificationTypeInterface;
use Terminal42\NotificationCenterBundle\Token\Definition\AnythingTokenDefinition;
use Terminal42\NotificationCenterBundle\Token\Definition\Factory\TokenDefinitionFactoryInterface;

class EventReservationNotificationType implements NotificationTypeInterface
{
    public const string NAME = 'event_reservation';

    public function __construct(private TokenDefinitionFactoryInterface $factory)
    {
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function getTokenDefinitions(): array
    {
        return [
            $this->factory->create(AnythingTokenDefinition::class, 'event_title', 'event_title'),
            $this->factory->create(AnythingTokenDefinition::class, 'event_startDate', 'event_startDate'),
            $this->factory->create(AnythingTokenDefinition::class, 'event_endDate', 'event_endDate'),
            $this->factory->create(AnythingTokenDefinition::class, 'event_startTime', 'event_startTime'),
            $this->factory->create(AnythingTokenDefinition::class, 'event_endTime', 'event_endTime'),
            $this->factory->create(AnythingTokenDefinition::class, 'event_max_places', 'event_max_places'),
            $this->factory->create(AnythingTokenDefinition::class, 'event_open_places', 'event_open_places'),
            $this->factory->create(AnythingTokenDefinition::class, 'reservation_firstname', 'reservation_firstname'),
            $this->factory->create(AnythingTokenDefinition::class, 'reservation_lastname', 'reservation_lastname'),
            $this->factory->create(AnythingTokenDefinition::class, 'reservation_street', 'reservation_street'),
            $this->factory->create(AnythingTokenDefinition::class, 'reservation_postal', 'reservation_postal'),
            $this->factory->create(AnythingTokenDefinition::class, 'reservation_email', 'reservation_email'),
            $this->factory->create(AnythingTokenDefinition::class, 'reservation_phone', 'reservation_phone'),
        ];
    }
}
