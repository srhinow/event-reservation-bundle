<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

$GLOBALS['BE_EVENT_RESERVATION']['PROPERTIES']['PUBLICSRC'] = 'bundles/srhinoweventreservation';

/*
 * -------------------------------------------------------------------------
 * Back end modules
 * -------------------------------------------------------------------------
 */
$GLOBALS['BE_MOD']['content']['calendar']['tables'][] = 'tl_event_reservations';
$GLOBALS['BE_MOD']['content']['calendar']['stylesheet'] = $GLOBALS['BE_EVENT_RESERVATION']['PROPERTIES']['PUBLICSRC'].'/css/be.css';
$GLOBALS['BE_MOD']['content']['calendar']['exportReservations'] = ['Srhinow\EventReservationBundle\BeReservationClass', 'exportReservations'];


/*
 * -------------------------------------------------------------------------
 * MODELS
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_MODELS']['tl_calendar_events'] = \Srhinow\EventReservationBundle\Model\CalendarEventsModel::class;
$GLOBALS['TL_MODELS']['tl_event_reservations'] = \Srhinow\EventReservationBundle\Model\EventReservationModel::class;

/*
 * -------------------------------------------------------------------------
 * Permissions are access settings for user and groups (fields in tl_user and tl_user_group)
 * -------------------------------------------------------------------------
 */
$GLOBALS['TL_PERMISSIONS'][] = 'event_reservationss';
$GLOBALS['TL_PERMISSIONS'][] = 'event_reservationsp';
