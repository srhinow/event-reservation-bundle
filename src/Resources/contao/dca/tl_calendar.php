<?php

declare(strict_types=1);

/**
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    event-reservation-bundle
 * @license    LGPL-3.0+
 */

/**
 * Palettes.
 */
$GLOBALS['TL_DCA']['tl_calendar']['palettes']['default'] .= ';{ereserv_legend:hide},ereserv_sender,ereserv_subject';

/**
 * Add fields to tl_calendar
 */
$GLOBALS['TL_DCA']['tl_calendar']['fields']['ereserv_subject'] = [
    'exclude' => true,
    'inputType' => 'text',
    'eval' => ['maxlength' => 255, 'tl_class' => 'clr long'],
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_calendar']['fields']['ereserv_sender'] = [
    'exclude' => true,
    'search' => true,
    'filter' => true,
    'inputType' => 'text',
    'eval' => ['rgxp' => 'email', 'maxlength' => 128, 'decodeEntities' => true, 'tl_class' => 'clr w50'],
    'sql' => "varchar(255) NOT NULL default ''",
];
