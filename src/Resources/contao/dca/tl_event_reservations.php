<?php

declare(strict_types=1);

/**
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    event-reservation-bundle
 * @license    LGPL-3.0+
 */

/*
 * Table tl_event_reservations.
 */
$GLOBALS['TL_DCA']['tl_event_reservations'] = [
    // Config
    'config' => [
        'dataContainer' => 'Table',
        'ptable' => 'tl_calendar_events',
        'enableVersioning' => true,
        'doNotCopyRecords' => true,
        'sql' => [
            'keys' => [
                'id' => 'primary',
                'pid' => 'index',
            ],
        ],
        'onsubmit_callback' => [
            ['srhinow.eventreservation.listener.event_reservations', 'sendReservationEmail'],
            ['srhinow.eventreservation.listener.event_reservations', 'updateReservationCount'],
        ],
        'ondelete_callback' => [
            ['srhinow.eventreservation.listener.event_reservations', 'updateReservationCount'],
        ],
    ],

    // List
    'list' => [
        'sorting' => [
            'mode' => 4,
            'fields' => ['status', 'firstname', 'lastname', 'email', 'library'],
            'panelLayout' => 'filter;sort,search,limit',
            'headerFields' => ['title', 'startDate', 'attendance', 'booked'],
            'child_record_callback' => ['srhinow.eventreservation.listener.event_reservations', 'listRecipient'],
            'child_record_class' => 'no_padding',
        ],
        'global_operations' => [
            'exportReservations' => [
                'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['exportReservations'],
                'href' => 'key=exportReservations',
                'class' => 'export_csv',
                'attributes' => 'onclick="Backend.getScrollOffset();"',
            ],
            'all' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"',
            ],
        ],
        'operations' => [
            'edit' => [
                'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif',
            ],
            'copy' => [
                'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif',
            ],
            'delete' => [
                'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"',
            ],
            'show' => [
                'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif',
            ],
        ],
    ],

    // Palettes
    'palettes' => [
        'default' => '
            {main_legend},firstname,lastname,street,postal,city;
            {contact_legend},phone,email,email2;{action_legend},status,notification,sendEmail;
            {more_legend},annotation
        ',
    ],

    // Fields
    'fields' => [
        'id' => [
            'label' => ['ID'],
            'search' => true,
            'sql' => 'int(10) unsigned NOT NULL auto_increment',
        ],
        'pid' => [
            'foreignKey' => 'tl_calendar_events.title',
            'sql' => "int(10) unsigned NOT NULL default '0'",
            'relation' => ['type' => 'belongsTo', 'load' => 'lazy'],
        ],
        'tstamp' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'modify' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'member_id' => [
            'sql' => "int(10) unsigned NOT NULL default '0'",
        ],
        'firstname' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'personal', 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'lastname' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'personal', 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'street' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 255, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'address', 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'postal' => [
            'label' => &$GLOBALS['TL_LANG']['tl_event_reservations']['postal'],
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['maxlength' => 32, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'address', 'tl_class' => 'w50'],
            'sql' => "varchar(32) NOT NULL default ''",
        ],
        'city' => [
            'exclude' => true,
            'filter' => true,
            'search' => true,
            'sorting' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 255, 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'address', 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'phone' => [
            'exclude' => true,
            'search' => true,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'maxlength' => 64, 'rgxp' => 'phone', 'feEditable' => true, 'feViewable' => true, 'feGroup' => 'contact', 'tl_class' => 'w50'],
            'sql' => "varchar(64) NOT NULL default ''",
        ],
        'email' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => true, 'rgxp' => 'email', 'feEditable' => true, 'feViewable' => true, 'maxlength' => 128, 'decodeEntities' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'email2' => [
            'exclude' => true,
            'search' => true,
            'sorting' => true,
            'flag' => 1,
            'inputType' => 'text',
            'eval' => ['mandatory' => false,'rgxp' => 'email', 'feEditable' => true, 'feViewable' => true, 'maxlength' => 128, 'decodeEntities' => true, 'tl_class' => 'w50'],
            'sql' => "varchar(255) NOT NULL default ''",
        ],
        'status' => [
            'exclude' => true,
            'filter' => true,
            'sorting' => true,
            'inputType' => 'select',
            'options' => ['' => 'nicht bearbeitet', '1' => 'bestätigt', '2' => 'abgelehnt', '3' => 'Warteliste'],
            'flag' => 1,
            'eval' => ['doNotCopy' => true, 'submitOnChange' => true, 'tl_class' => 'clr w50'],
            'save_callback' => [
                ['srhinow.eventreservation.listener.event_reservations', 'UpdateEntry'],
            ],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'notification' => [
            'exclude' => true,
            'inputType' => 'select',
            'eval' => ['includeBlankOption' => true, 'chosen' => true, 'tl_class' => 'clr w50'],
            'sql' => ['type' => 'integer', 'default' => 0, 'unsigned' => true],
        ],
        'statusDate' => [
            'filter' => true,
            'sorting' => true,
            'flag' => 8,
            'eval' => ['rgxp' => 'datim'],
            'sql' => "varchar(10) NOT NULL default ''",
        ],
        'sendEmail' => [
            'inputType' => 'checkbox',
            'flag' => 11,
            'eval' => ['tl_class' => 'clr'],
            'sql' => "char(1) NOT NULL default ''",
        ],
        'annotation' => array
        (
            'search'                  => true,
            'inputType'               => 'textarea',
            'eval'                    => array('style'=>'height:60px;', 'tl_class'=>'clr','decodeEntities'=>true),
            'explanation'             => 'insertTags',
            'sql'                     => "text NULL"
        ),
    ],
];
