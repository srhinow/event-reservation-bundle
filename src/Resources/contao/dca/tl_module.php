<?php

declare(strict_types=1);

/**
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    event-reservation-bundle
 * @license    LGPL-3.0+
 */

/**
 * Add palettes to tl_module.
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['eventreservationreader'] = '{title_legend},name,headline,type;{config_legend},cal_calendar;{template_legend:hide},cal_template,customTpl,imgSize;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space;{registerform_legend},jumpTo';
$GLOBALS['TL_DCA']['tl_module']['palettes']['eventreservationform'] = '{title_legend},name,headline,type;{config_legend},cal_calendar;{template_legend:hide},cal_template,customTpl;{protected_legend:hide},protected;{expert_legend:hide},cssID,space;{registerform_legend},jumpTo';

/**
 * Add fields to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['ereserv_subject'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_module']['ereserv_subject'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => ['maxlength' => 255, 'tl_class' => 'clr long'],
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_module']['fields']['ereserv_email'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_module']['ereserv_email'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => ['rgxp' => 'email', 'maxlength' => 128, 'decodeEntities' => true, 'tl_class' => 'clr w50'],
    'sql' => "varchar(255) NOT NULL default ''",
];
