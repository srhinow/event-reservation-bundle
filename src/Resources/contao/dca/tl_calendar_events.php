<?php

declare(strict_types=1);

/**
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    event-reservation-bundle
 * @license    LGPL-3.0+
 */

$GLOBALS['TL_DCA']['tl_calendar_events']['config']['ctable'][] = 'tl_event_reservations';
$GLOBALS['TL_DCA']['tl_calendar_events']['config']['onsubmit_callback'][] = [
    'srhinow.eventreservation.listener.calendar_events',
    'updateReservationCount',
];
$GLOBALS['TL_DCA']['tl_calendar_events']['list']['sorting']['headerFields'] = [
    'title', 'jumpTo', 'tstamp', 'protected', 'allowComments', 'makeFeed', 'attendance', 'booked',
];
$GLOBALS['TL_DCA']['tl_calendar_events']['list']['sorting']['child_record_callback'] = [
    'srhinow.eventreservation.listener.calendar_events',
    'listEvents',
];
$GLOBALS['TL_DCA']['tl_calendar_events']['list']['operations']['reservations'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_calendar_events']['reservations'],
    'href' => 'table=tl_event_reservations',
    'icon' => $GLOBALS['BE_EVENT_RESERVATION']['PROPERTIES']['PUBLICSRC'] . '/icons/users_two_16.gif',
];

/**
 * Palettes
 */
$GLOBALS['TL_DCA']['tl_calendar_events']['palettes']['default']
    .= ';{reserv_legend},dontShowReserveLink,attendance,ereserv_formto_email,ereserv_formto_subject';

/**
 * Add fields to tl_calendar_events
 */

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['dontShowReserveLink'] = [
    'exclude' => true,
    'inputType' => 'checkbox',
    'eval' => array('tl_class' => 'clr m12'),
    'sql' => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['attendance'] = [
    'exclude' => true,
    'search' => true,
    'sorting' => true,
    'flag' => 1,
    'inputType' => 'text',
    'eval' => ['mandatory' => true, 'maxlength' => 255, 'rgxp' => 'digit', 'tl_class' => 'clr w50'],
    'sql' => "smallint(5) unsigned NOT NULL default '0'",
];
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ereserv_formto_subject'] = [
    'exclude' => true,
    'inputType' => 'text',
    'eval' => ['maxlength' => 255, 'tl_class' => 'clr long'],
    'sql' => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ereserv_formto_email'] = [
    'exclude' => true,
    'search' => true,
    'sorting' => true,
    'flag' => 11,
    'inputType' => 'text',
    'eval' => ['decodeEntities' => true, 'maxlength' => 128, 'tl_class' => 'clr w50'],
    'sql' => "varchar(255) NOT NULL default ''",
];


$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['booked'] = [
    'sql' => "smallint(1) NOT NULL default '0'",
];
