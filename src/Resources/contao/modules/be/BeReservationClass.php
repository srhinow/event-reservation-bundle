<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

namespace Srhinow\EventReservationBundle;

use Contao\Backend;
use Contao\CalendarEventsModel;
use Contao\Database;
use Contao\Input;
use Contao\Message;
use Contao\System;
use Psr\Log\LogLevel;

class BeReservationClass extends Backend
{
    /**
     * Export Invoices.
     */
    public function exportReservations()
    {
        if ('export_reservations' === Input::post('FORM_SUBMIT')) {
            $pid = Input::post('pid');

            //Check the reference-id
            if ($pid < 1) {
                Message::addInfo($GLOBALS['TL_LANG']['tl_event_reservations']['MESSAGE']['no_id']);
                $logger = System::getContainer()->get('monolog.logger.contao');
                $logger->log(LogLevel::ALERT, $GLOBALS['TL_LANG']['tl_event_reservations']['MESSAGE']['no_id']);
                $this->redirect(str_replace('&key=exportReservations', '', $this->Environment->request));
            }

            $eventObj = CalendarEventsModel::findByIdOrAlias($pid);

            //get DB-Fields as arrays
            $arrFields = [];
            $reservation_fields = Database::getInstance()->listFields('tl_event_reservations');

            foreach ($reservation_fields as $field) {
                //exclude index Fields
                if ('index' === $field['type']) {
                    continue;
                }

                //exclude eny fields
                if (\in_array($field['name'], ['id', 'pid', 'member_id', 'sendEmail'], true)) {
                    continue;
                }
                $arrFields[] = $field['name'];
            }

            //set handle from file
            $seperators = ['comma' => ',', 'semicolon' => ';', 'tabulator' => "\t", 'linebreak' => "\n"];

            // get records
            $arrExport = [];
            $objRow = Database::getInstance()->prepare('SELECT '.implode(', ', $arrFields).' FROM `tl_event_reservations` WHERE `pid` = ?')
                         ->execute($eventObj->id)
            ;

            //Check the reference-id
            if ($objRow->numRows < 1) {
                Message::addInfo($GLOBALS['TL_LANG']['tl_event_reservations']['MESSAGE']['no_data']);
                $logger = System::getContainer()->get('monolog.logger.contao');
                $logger->log(LogLevel::ALERT, $GLOBALS['TL_LANG']['tl_event_reservations']['MESSAGE']['no_data']);
                $this->redirect(str_replace('&key=exportReservations', '', $this->Environment->request));
            }

            $arrExport = $objRow->fetchAllAssoc();

            // start output
            $exportFile = 'export_'.$eventObj->alias.'_'.date('Ymd-Hi');

            header('Content-Type: application/csv');
            header('Content-Transfer-Encoding: binary');
            header('Content-Disposition: attachment; filename="'.$exportFile.'.csv"');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Expires: 0');

            $output = '"'.implode('"'.$seperators[Input::post('separator')].'"', array_keys($arrExport[0])).'"'."\n";

            foreach ($arrExport as $export) {
                $export['state'] = $GLOBALS['TL_LANG']['tl_event_reservations']['state_options'][$export['state']];
                $output .= '"'.implode('"'.$seperators[Input::post('separator')].'"', str_replace('"', '""', $export)).'"'."\n";
            }

            echo $output;
            exit();
        }

        // Return the form
        return '<div class="tl_content">
		    <div id="tl_buttons">
		    <a href="'.ampersand(str_replace('&key=exportReservations', '', $this->Environment->request)).'" class="header_back" title="'.specialchars($GLOBALS['TL_LANG']['MSC']['backBT']).'" accesskey="b">'.$GLOBALS['TL_LANG']['MSC']['backBT'].'</a>
		    </div>
		    
		    <h2 class="sub_headline">'.$GLOBALS['TL_LANG']['tl_event_reservations']['exportReservations'][1].'</h2>'.$this->getMessages().'
		    
		    <form action="'.ampersand($this->Environment->request, true).'" id="export_reservations" class="tl_form" method="post">
		    <div class="tl_formbody_edit">
			<input type="hidden" name="FORM_SUBMIT" value="export_reservations" />
			<input type="hidden" name="REQUEST_TOKEN" value="'.REQUEST_TOKEN.'" />
			<input type="hidden" name="pid" value="'.$this->Input->get('id').'" />
			<fieldset class="tl_box">
			    <label for="separator">'.$GLOBALS['TL_LANG']['MSC']['separator'][0].'</label>
			    <select name="separator" id="separator" class="tl_select" onfocus="Backend.getScrollOffset();">
                    <option value="comma">'.$GLOBALS['TL_LANG']['MSC']['comma'].'</option>
                    <option value="semicolon">'.$GLOBALS['TL_LANG']['MSC']['semicolon'].'</option>
                    <option value="tabulator">'.$GLOBALS['TL_LANG']['MSC']['tabulator'].'</option>
                    <option value="linebreak">'.$GLOBALS['TL_LANG']['MSC']['linebreak'].'</option>
			    </select>'.(('' !== $GLOBALS['TL_LANG']['MSC']['separator'][1]) ? '<p class="tl_help tl_tip">'.$GLOBALS['TL_LANG']['MSC']['separator'][1].'</p>' : '').' 
			</fieldset>
		    </div>
		    
		    <div class="tl_formbody_submit">
		    
		    <div class="tl_submit_container">
		      <input type="submit" name="save" id="save" class="tl_submit" accesskey="s" value="'.specialchars($GLOBALS['TL_LANG']['tl_event_reservations']['exportCSV'][0]).'" />
		    </div>
		    
		    </div>
		    </form></div>';
    }
}
