<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

if (!\defined('TL_ROOT')) {
    die('You cannot access this file directly!');
}

/*
 * Contao Open Source CMS
 * Copyright (C) 2005-2012 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Sven Rhinow 2012
 * @author     Sven Rhinow <http://www.sr-tag.de>
 * @package    eventReservation
 * @license    LGPL
 * @filesource
 */

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['firstname'] = ['Vorname', 'Bitte geben Sie den Vornamen ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['lastname'] = ['Nachname', 'Bitte geben Sie den Nachnamen ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['library'] = ['Bibliothek', 'Bitte geben Sie den Typ der Bibliothek ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['libtype'] = ['Bibliothekstyp', 'Bitte geben Sie das Bundesland ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['street'] = ['Straße', 'Bitte geben Sie den Straßennamen und die Hausnummer ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['postal'] = ['Postleitzahl', 'Bitte geben Sie die Postleitzahl ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['city'] = ['Ort', 'Bitte geben Sie den Namen des Ortes ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['state'] = ['Bundesland', 'Bitte geben Sie das Bundesland ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['phone'] = ['Telefonnummer', 'Bitte geben Sie die Telefonnummer ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['email'] = ['E-Mail-Adresse', 'Bitte geben Sie die E-Mail-Adresse des Abonnenten ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['sendEmail'] = ['E-Mail senden', 'Wenn diese checkbox aktiviert wird, wird eine Reservierungs-E-Mail versendet.'];

$GLOBALS['TL_LANG']['tl_event_reservations']['subject'] = ['E-Mail-Betreff', 'Hier können Sie den Betreff der E-Mail eintragen.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['html_email'] = ['HTML-Version der E-Mail', 'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##'];
$GLOBALS['TL_LANG']['tl_event_reservations']['text_email'] = ['Text-Version der E-Mail',  'Platzhalter: Startdatum=##startDate##, Enddatum=##endDate##, Startzeit=##startTime##, Veranstaltungsende=##endTime##, Vorname=##firstname##, Nachname=##lastname##, Bibliothek=##library##, Straße=##street##, Postleitzahl=##postal##, Ort=##city##, Telefonnummer=##phone##, E-Mail-Adresse=##email##'];

$GLOBALS['TL_LANG']['tl_event_reservations']['statusDate'] = ['Bestätigungssdatum', 'Das Datum der Bestätigung.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['status'] = ['Teilnahme bestätigen / ablehnen', 'Teilnahme bestätigen oder ablehnen und dazu gehörige E-Mail senden'];
$GLOBALS['TL_LANG']['tl_event_reservations']['annotation'] = ['Anmerkung', ''];
/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['main_legend'] = 'Haupteinstellungen';
$GLOBALS['TL_LANG']['tl_event_reservations']['contact_legend'] = 'Kontaktdaten';
$GLOBALS['TL_LANG']['tl_event_reservations']['action_legend'] = 'Aktionen';
$GLOBALS['TL_LANG']['tl_event_reservations']['more_legend'] = 'weitere Einstellungen';

/*
 * Reference
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['confirm'] = '%s neue Abonnenten wurden importiert.';
$GLOBALS['TL_LANG']['tl_event_reservations']['invalid'] = '%s ungültige Einträge wurden übersprungen.';
$GLOBALS['TL_LANG']['tl_event_reservations']['subscribed'] = 'registriert am %s';
$GLOBALS['TL_LANG']['tl_event_reservations']['manually'] = 'manuell hinzugefügt';
$GLOBALS['TL_LANG']['tl_event_reservations']['participation_accepted'] = 'Teilnahme bestätigt';
$GLOBALS['TL_LANG']['tl_event_reservations']['participation_rejected'] = 'Teilnahme abgelehnt';
$GLOBALS['TL_LANG']['tl_event_reservations']['participation_no_accepted'] = 'Teilnahme nicht bestätigt';

$GLOBALS['TL_LANG']['tl_event_reservations']['state_options'] = [
    '' => '--',
    'bw' => 'Baden-Württemberg',
    'bayern' => 'Bayern',
    'berlin' => 'Berlin',
    'brandenburg' => 'Brandenburg',
    'bremen' => 'Bremen',
    'hamburg' => 'Hamburg',
    'hessen' => 'Hessen',
    'meckpom' => 'Mecklenburg-Vorpommern',
    'niedersachsen' => 'Niedersachsen',
    'nw' => 'Nordrhein-Westfalen',
    'rheinpfalz' => 'Rheinland-Pfalz',
    'saarland' => 'Saarland',
    'sachsen' => 'Sachsen',
    'sachsenanhalt' => 'Sachsen-Anhalt',
    'sh' => 'Schleswig-Holstein',
    'tgen' => 'Thüringen',
    'notgerman' => 'nicht aus Deutschland',
];

$GLOBALS['TL_LANG']['tl_event_reservations']['libtype_options'] = [
    '' => '--',
    'ÖB' => 'öffentliche Bibliothek',
    'WB' => 'wissenschaftliche Bibliothek',
    'SB' => 'Schulbibliothek',
];
/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['new'] = ['Reservierung hinzufügen', 'Einen neuen Abonnenten hinzufügen'];
$GLOBALS['TL_LANG']['tl_event_reservations']['show'] = ['Reservierungdetails', 'Details des Abonnenten ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_event_reservations']['edit'] = ['Reservierung bearbeiten', 'Abonnenten ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_event_reservations']['copy'] = ['Reservierung duplizieren', 'Abonnenten ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_event_reservations']['delete'] = ['Reservierung löschen', 'Abonnenten ID %s löschen'];
$GLOBALS['TL_LANG']['tl_event_reservations']['editheader'] = ['Verteiler bearbeiten', 'Die Verteiler-Einstellungen bearbeiten'];
$GLOBALS['TL_LANG']['tl_event_reservations']['toggle'] = ['Reservierung aktivieren/deaktivieren', 'Reservierung ID %s aktivieren/deaktivieren'];
$GLOBALS['TL_LANG']['tl_event_reservations']['import'] = ['CSV-Import', 'Reservierungen aus einer CSV-Datei importieren'];

/*
* Export as CSV
*/
$GLOBALS['TL_LANG']['tl_event_reservations']['exportReservations'] = ['CSV-Export', 'Reservierungen als CSV exportieren'];
$GLOBALS['TL_LANG']['tl_event_reservations']['exportCSV'] = ['starte Export', ''];
