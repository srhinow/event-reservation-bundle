<?php
declare(strict_types=1);

$GLOBALS['TL_LANG']['tl_nc_notification']['type']['event_reservation'] = ["Event-Reservierung"];

// Messages
$GLOBALS['TL_LANG']['MSC']['messages']['nc_send_succesfully'] = "Die E-Mail wurde erfolgreich gesendet";
$GLOBALS['TL_LANG']['MSC']['messages']['nc_send_error'] = "Beim senden der E-Mail ist nicht erfolgreich versendet worden. Bitte versuchen Sie es erneut.";
