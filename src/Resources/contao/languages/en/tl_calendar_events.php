<?php

declare(strict_types=1);

/**
 * @copyright  Sven Rhinow <https://www.sr-tag.de>
 * @author     Sven Rhinow
 * @package    event-reservation-bundle
 * @license    LGPL-3.0+
 */

/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_calendar_events']['reserv_legend'] = 'Reservierungs-Einstellungen';

/**
 *  Fields
 */
$GLOBALS['TL_LANG']['tl_calendar_events']['attendance'] = ['max. Teilnehmerzahl', 'Tragen Sie hier die Teilnehmerzahl ein, die sich maximal anmelden können.'];
$GLOBALS['TL_LANG']['tl_calendar_events']['booked'] = ['gebucht', ''];

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_calendar_events']['new'] = ['Neue Veranstaltung', 'Eine neue Veranstaltung erstellen'];
$GLOBALS['TL_LANG']['tl_calendar_events']['show'] = ['Details der Veranstaltung', 'Details der Veranstaltung ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_calendar_events']['edit'] = ['Veranstaltung bearbeiten', 'Veranstaltung ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_calendar_events']['copy'] = ['Veranstaltung duplizieren', 'Veranstaltung ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_calendar_events']['cut'] = ['Veranstaltung verschieben', 'Veranstaltung ID %s verschieben'];
$GLOBALS['TL_LANG']['tl_calendar_events']['delete'] = ['Veranstaltung löschen', 'Veranstaltung ID %s löschen'];
$GLOBALS['TL_LANG']['tl_calendar_events']['toggle'] = ['Veranstaltung veröffentlichen/unveröffentlichen', 'Event ID %s veröffentlichen/unveröffentlichen'];
$GLOBALS['TL_LANG']['tl_calendar_events']['reservations'] = ['Reservierungen bearbeiten', 'Reservierungen der Veranstaltung ID %s bearbeiten'];

$GLOBALS['TL_LANG']['tl_calendar_events']['ereserv_formto_email'] = ['Ziel-Email der Reservierungsanfragen', 'Tragen Sie hier die Emailadresse ein, welche bei einer Reservierungsanfrage auf der Website die Meldung erhalten soll.'];

