<?php

declare(strict_types=1);

/**
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_calendar']['ereserv_subject'] = ['E-Mail-Betreff', 'Betreff der Reservierungsanfrage'];
$GLOBALS['TL_LANG']['tl_calendar']['ereserv_sender'] = ['Absenderadresse', 'Hier können Sie eine individuelle Absenderadresse eingeben.'];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_calendar']['ereserv_legend'] = 'Veranstaltungs-Reservierungen Einstellungen';

/*
* Default-Values
*/
$GLOBALS['TL_LANG']['tl_calendar']['ereserv_confirmed_subject_default'] = 'Die Teilnahme an der Veranstaltung wird bestätigt';
$GLOBALS['TL_LANG']['tl_calendar']['ereserv_rejected_subject_default'] = 'Die Teilnahme an der Veranstaltung wird abgelehnt';
