<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

$GLOBALS['TL_LANG']['MOD']['calendar'] = ['Veranstaltungen', 'Veranstaltungen verwalten und als Kalender oder Eventliste ausgeben.'];

/*
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['events'] = 'Veranstaltungen';
$GLOBALS['TL_LANG']['FMD']['eventreservationreader'] = ['Veranstaltung mit Reservierungslink', 'Detailansicht mit Reservierungslink.'];
$GLOBALS['TL_LANG']['FMD']['eventreservationform'] = ['Reservierungsformular', 'Ein Reservierungsformular für eine Veranstaltung.'];
