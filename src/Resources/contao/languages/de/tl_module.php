<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

if (!\defined('TL_ROOT')) {
    die('You cannot access this file directly!');
}

/*
 * Contao Open Source CMS
 * Copyright (C) 2005-2012 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Sven Rhinow 2012
 * @author     Sven Rhinow <http://www.sr-tag.de>
 * @package    eventReservation
 * @license    LGPL
 * @filesource
 */

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_module']['ereserv_subject'] = ['E-Mail-Betreff', 'Betreff der Reservierungsanfrage'];
$GLOBALS['TL_LANG']['tl_module']['ereserv_email'] = ['E-Mail-Adresse ', 'Eingang einer Reservierung'];

$GLOBALS['TL_LANG']['tl_module']['registerform_legend'] = 'Registrierungs-Einstellungen';
