<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

$GLOBALS['TL_LANG']['tl_event_reservations']['firstname'] = ['Vorname', 'Bitte geben Sie den Vornamen ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['lastname'] = ['Nachname', 'Bitte geben Sie den Nachnamen ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['library'] = ['Bibliothek', 'Bitte geben Sie den Typ der Bibliothek ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['libtype'] = ['Bibliothekstyp', 'Bitte geben Sie das Bundesland ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['street'] = ['Straße', 'Bitte geben Sie den Straßennamen und die Hausnummer ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['postal'] = ['Postleitzahl', 'Bitte geben Sie die Postleitzahl ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['city'] = ['Ort', 'Bitte geben Sie den Namen des Ortes ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['state'] = ['Bundesland', 'Bitte geben Sie das Bundesland ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['phone'] = ['Telefonnummer', 'Bitte geben Sie die Telefonnummer ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['email'] = ['E-Mail-Adresse', 'Bitte geben Sie die E-Mail-Adresse des Abonnenten ein.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['email2'] = [' abweichende E-Mail-Adresse für Rechnung', 'Bitte geben Sie die E-Mail-Adresse für eine Rechnungen ein.'];

$GLOBALS['TL_LANG']['tl_event_reservations']['notification'] = ['Benachrichtigung', 'Welche Einstellung aus dem Notification-Center soll versendet werden.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['statusDate'] = ['Bestätigungssdatum', 'Das Datum der Bestätigung.'];
$GLOBALS['TL_LANG']['tl_event_reservations']['status'] = ['Teilnahme bestätigen / ablehnen', 'Teilnahme bestätigen oder ablehnen und dazu gehörige E-Mail senden'];
$GLOBALS['TL_LANG']['tl_event_reservations']['annotation'] = ['Anmerkung', ''];

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['main_legend'] = 'Haupteinstellungen';
$GLOBALS['TL_LANG']['tl_event_reservations']['contact_legend'] = 'Kontaktdaten';
$GLOBALS['TL_LANG']['tl_event_reservations']['action_legend'] = 'Aktionen';
$GLOBALS['TL_LANG']['tl_event_reservations']['more_legend'] = 'weitere Einstellungen';

/*
 * Formular
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['form_submit_title'] = 'Reservierungsanfrage senden';
/*
 * Reference
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['confirm'] = '%s neue Abonnenten wurden importiert.';
$GLOBALS['TL_LANG']['tl_event_reservations']['invalid'] = '%s ungültige Einträge wurden übersprungen.';
$GLOBALS['TL_LANG']['tl_event_reservations']['subscribed'] = 'registriert am %s';
$GLOBALS['TL_LANG']['tl_event_reservations']['manually'] = 'manuell hinzugefügt';
$GLOBALS['TL_LANG']['tl_event_reservations']['participation_accepted'] = 'Teilnahme bestätigt';
$GLOBALS['TL_LANG']['tl_event_reservations']['participation_rejected'] = 'Teilnahme abgelehnt';
$GLOBALS['TL_LANG']['tl_event_reservations']['participation_waiting_list'] = 'auf Warteliste';
$GLOBALS['TL_LANG']['tl_event_reservations']['participation_no_accepted'] = 'Teilnahme nicht bestätigt';

$GLOBALS['TL_LANG']['tl_event_reservations']['state_options'] = [
    '' => '--',
    'bw' => 'Baden-Württemberg',
    'bayern' => 'Bayern',
    'berlin' => 'Berlin',
    'brandenburg' => 'Brandenburg',
    'bremen' => 'Bremen',
    'hamburg' => 'Hamburg',
    'hessen' => 'Hessen',
    'meckpom' => 'Mecklenburg-Vorpommern',
    'niedersachsen' => 'Niedersachsen',
    'nw' => 'Nordrhein-Westfalen',
    'rheinpfalz' => 'Rheinland-Pfalz',
    'saarland' => 'Saarland',
    'sachsen' => 'Sachsen',
    'sachsenanhalt' => 'Sachsen-Anhalt',
    'sh' => 'Schleswig-Holstein',
    'tgen' => 'Thüringen',
    'notgerman' => 'nicht aus Deutschland',
];

$GLOBALS['TL_LANG']['tl_event_reservations']['libtype_options'] = [
    '' => '--',
    'ÖB' => 'öffentliche Bibliothek',
    'WB' => 'wissenschaftliche Bibliothek',
    'SB' => 'Schulbibliothek',
];

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['new'] = ['Reservierung hinzufügen', 'Einen neuen Abonnenten hinzufügen'];
$GLOBALS['TL_LANG']['tl_event_reservations']['show'] = ['Reservierungdetails', 'Details des Abonnenten ID %s anzeigen'];
$GLOBALS['TL_LANG']['tl_event_reservations']['edit'] = ['Reservierung bearbeiten', 'Abonnenten ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_event_reservations']['copy'] = ['Reservierung duplizieren', 'Abonnenten ID %s duplizieren'];
$GLOBALS['TL_LANG']['tl_event_reservations']['delete'] = ['Reservierung löschen', 'Abonnenten ID %s löschen'];
$GLOBALS['TL_LANG']['tl_event_reservations']['editheader'] = ['Verteiler bearbeiten', 'Die Verteiler-Einstellungen bearbeiten'];
$GLOBALS['TL_LANG']['tl_event_reservations']['toggle'] = ['Reservierung aktivieren/deaktivieren', 'Reservierung ID %s aktivieren/deaktivieren'];
$GLOBALS['TL_LANG']['tl_event_reservations']['import'] = ['CSV-Import', 'Reservierungen aus einer CSV-Datei importieren'];

/*
* Export as CSV
*/
$GLOBALS['TL_LANG']['tl_event_reservations']['exportReservations'] = ['CSV-Export', 'Reservierungen als CSV exportieren'];
$GLOBALS['TL_LANG']['tl_event_reservations']['exportCSV'] = ['starte Export', ''];

/*
 * Messages
 */
$GLOBALS['TL_LANG']['tl_event_reservations']['MESSAGE']['no_id'] = 'Es wurde keine ID als Daten-Referenz angegeben.';
$GLOBALS['TL_LANG']['tl_event_reservations']['MESSAGE']['no_data'] = 'Es sind keine Daten zum exportieren vorhanden.';
