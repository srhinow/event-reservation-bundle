<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

namespace Srhinow\EventReservationBundle\Model;

use Contao\Model;

class EventReservationModel extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected static $strTable = 'tl_event_reservations';
}
