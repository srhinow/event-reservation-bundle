<?php

declare(strict_types=1);

/*
 *  * @copyright  Sven Rhinow <https://www.sr-tag.de>
 *  * @author     Sven Rhinow
 *  * @package    event-reservation-bundle
 *  * @license    LGPL-3.0+
 */

namespace Srhinow\EventReservationBundle\Model;

use Contao\Date;

class CalendarEventsModel extends \Contao\CalendarEventsModel
{
    /**
     * Find a published event by ID or alias.
     *
     * @param mixed $varId      The numeric ID or alias name
     * @param array $arrOptions An optional options array
     *
     * @return CalendarEventsModel|null The model or null if there is no event
     */
    public static function findPublishedByIdOrAlias($varId, array $arrOptions = [])
    {
        $t = static::$strTable;
        $arrColumns = !preg_match('/^[1-9]\d*$/', $varId) ? ["BINARY $t.alias=?"] : ["$t.id=?"];

        if (!static::isPreviewMode($arrOptions)) {
            $time = Date::floorToMinute();
            $where = "$t.published='1'";
            $where .= " AND ($t.start='' OR $t.start<='$time')";
            $where .= " AND ($t.stop='' OR $t.stop>'$time')";
            $arrColumns[] = $where;
        }

        return static::findOneBy($arrColumns, $varId, $arrOptions);
    }
}
