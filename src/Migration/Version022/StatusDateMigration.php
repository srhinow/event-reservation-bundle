<?php

declare(strict_types=1);

/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 06.01.25
 */

namespace Srhinow\EventReservationBundle\Migration\Version022;

use Contao\CoreBundle\Migration\AbstractMigration;
use Contao\CoreBundle\Migration\MigrationResult;
use Doctrine\DBAL\Connection;

class StatusDateMigration extends AbstractMigration
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function shouldRun(): bool
    {
        $schemaManager = $this->connection->createSchemaManager();

        if (!$schemaManager->tablesExist(['tl_event_reservations'])) {
            return false;
        }

        $columns = $schemaManager->listTableColumns('tl_event_reservations');

        return
            isset($columns['addedon']) &&
            !isset($columns['statusdate']);
    }

    public function run(): MigrationResult
    {
        $this->connection->executeQuery("
            ALTER TABLE
                tl_event_reservations
            ADD
                statusDate varchar(10) NOT NULL default ''
        ");

        $stmt = $this->connection->prepare("
            UPDATE
                tl_event_reservations
            SET
                statusDate = addedOn
            WHERE LENGTH(addedOn) > 0
        ");

        $stmt->execute();

        return $this->createResult(
            true,
            'Set statusDate values from addedOn in tl_event_reservations.'
        );
    }
}
