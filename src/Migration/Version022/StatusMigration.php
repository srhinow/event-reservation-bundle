<?php

declare(strict_types=1);

namespace Srhinow\EventReservationBundle\Migration\Version022;

use Contao\CoreBundle\Migration\AbstractMigration;
use Contao\CoreBundle\Migration\MigrationResult;
use Doctrine\DBAL\Connection;

/**
 * Created by bzn-cms_contao4.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 06.01.25
 */
class StatusMigration extends AbstractMigration
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function shouldRun(): bool
    {
        $schemaManager = $this->connection->createSchemaManager();

        if (!$schemaManager->tablesExist(['tl_event_reservations'])) {
            return false;
        }

        $columns = $schemaManager->listTableColumns('tl_event_reservations');

        return
            isset($columns['accepted']) &&
            !isset($columns['status']);
    }

    public function run(): MigrationResult
    {
        $this->connection->executeQuery("
            ALTER TABLE
                tl_event_reservations
            ADD
                status char(1) NOT NULL DEFAULT ''
        ");

        $stmt = $this->connection->prepare("
            UPDATE
                tl_event_reservations
            SET
                status = accepted
            WHERE LENGTH(accepted) > 0
        ");

        $stmt->execute();

        return $this->createResult(
            true,
            'Set status values from accepted in tl_event_reservations.'
        );
    }
}
